<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://hubshark.com
 * @since      1.0.0
 *
 * @package    Wp_Tut
 * @subpackage Wp_Tut/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Wp_Tut
 * @subpackage Wp_Tut/includes
 * @author     hyperclock (aka JMColeman) <hyperclock@hubshark.com>
 */
class Wp_Tut_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
