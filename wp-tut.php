<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://hubshark.com
 * @since             1.0.0
 * @package           Wp_Tut
 *
 * @wordpress-plugin
 * Plugin Name:       wpTut
 * Plugin URI:        https://gitlab.com/HubShark/wpTut
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            hyperclock (aka JMColeman)
 * Author URI:        http://hubshark.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wp-tut
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-wp-tut-activator.php
 */
function activate_wp_tut() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wp-tut-activator.php';
	Wp_Tut_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-wp-tut-deactivator.php
 */
function deactivate_wp_tut() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wp-tut-deactivator.php';
	Wp_Tut_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_wp_tut' );
register_deactivation_hook( __FILE__, 'deactivate_wp_tut' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wp-tut.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_wp_tut() {

	$plugin = new Wp_Tut();
	$plugin->run();

}
run_wp_tut();
