<?php

/**
 * Fired during plugin activation
 *
 * @link       http://hubshark.com
 * @since      1.0.0
 *
 * @package    Wp_Tut
 * @subpackage Wp_Tut/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wp_Tut
 * @subpackage Wp_Tut/includes
 * @author     hyperclock (aka JMColeman) <hyperclock@hubshark.com>
 */
class Wp_Tut_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
